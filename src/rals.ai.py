# Requires python >=3.5

# To install the discord library, make sure you have the pip module in python 
# And run python3 -m pip install -U discord.py
# and that should take care of it!
import discord
import time
import asyncio

# Importing commands
from discord.ext import commands

# Opening the token file which contains our token 
tokenfile = open("/etc/rals.ai/tokenfile","r")
# Saving the token to the varriable token
token = tokenfile.read()

# To double check that this is working uncomment the following line
# and see if the token appearing in stdout is the right token
#print(token)

# Rals.ai by default uses the r. prefix for commands
client = commands.Bot(command_prefix = "r.")

##### Perform after conection actions 
@client.event
async def on_ready():
    # The bot will now "play" this game on discord 
    await client.change_presence(game=discord.Game
            (name=("""Hugging those that use r.help""")))
    # Output to stdout that the bot is ready!
    print(("""Rals.ai is ready!"""))

##### Rals.ai Commands

### r.flirt
@client.command(pass_context = True)
async def flirt(ctx):
    # Tell the sender how beautiful they are
    await client.say(("""<@%s>, your beauty is just... transcendant...""" % 
        str(ctx.message.author.id)))
    # Wait 4 seconds for comedic timing
    time.sleep(4)
    # Be embarrased 
    await client.say(("""Oh, oh dear! Wait!!"""))

### Ping
@client.command()
async def ping():
    await client.say(("""Pong!"""))

### Hugs 
@client.command()
async def hug():
    await client.say(("""Hugs! :heart: \n https://pre00.deviantart.net/"""
        """2b7f/th/pre/i/2018/306/d/a/hug_ralsei_by_"""
        """mcwitherzberry-dcqynhd.png""")) 

### PRAISE the floof! 
@client.command()
async def PRAISE():
    await client.say(("""P R A I S E  T H E  F L O O F """
    """<:ralseiBlush:507685585947983882>"""))
            #NOTE: Server specific emotes work everywhere!
            # Well as long as the bot is in that server to begin with 
            
### praise the floof! 
@client.command()
async def praise():
    await client.say(("""P R A I S E  T H E  F L O O F """
    """<:ralseiBlush:507685585947983882>"""))
            #NOTE: Server specific emotes work everywhere!
            # Well as long as the bot is in that server to begin with 


### r.honey
@client.command()
async def honey():
    await client.say(("""*Hugs you*   Hello, Honey! :heart:"""))

### r.moneyhugs
@client.command()
async def moneyhug():
    await client.say(("""You don't need money to get me to hug you~"""))
    
### r.pat
@client.command()
async def pat():
    await client.say(("""Oh! Uhhh, Thanks!"""
    """<:ralseiBlush:507685585947983882>"""))
    
### r.magic
@client.command()
async def magic():
    await client.say(("""Huh? You want to show me some magic...? """ 
    """But that's just a mirror!"""))
    time.sleep(3)
    await client.say(("""..."""))
    time.sleep(1)
    await client.say(("""Oh! Goodness!"""
    """<:ralseiBlush:507685585947983882>"""))
    
### r.tail
@client.command()
async def tail():
    await client.say(("""Don't lewd the floof"""))

### r.lewd
@client.command()
async def lewd():
    await client.say(("""...Please no."""))
    
### r.love
@client.command()
async def love():
    await client.say(("""Thank you! I love you too!"""))
    
### r.owo
@client.command()
async def owo():
    await client.say(("""What's this?"""))

### r.police
@client.command()
async def 911():
    await client.say(("""PUT YOUR HANDS IN THE AIR! *Points stuffed animal*"""))
    
### r.ruffle
@client.command()
async def ruffle():
    await client.say(("""<@%s> lovingly ruffled the floof."""))
    time.sleep(2)
    await client.say(("""<:ralseiBlush:507685585947983882>"""
    """* Oh, haha... :heart:"""))
    
### Hug Party! 

## We need to asign these first but outside of the client.command blocks

party_started = False   # As a check for r.partystart otherwise we'll
                        # index outside a list

party_name_list = [""]  # To keep everyone's IDs saved

party_channel = ""      # What channel the party is on (it stays on the 
                        # starting channel

## r.party (the join command)
@client.command(pass_context = True)
async def party(ctx):
    # Set our globals
    global party_started
    global party_name_list
    global party_channel
    
    # Is there already a party going on?
    if party_started == True:
        party_name_list.append(str(ctx.message.author.id))  # If so,
                                                            # add whoever 
                                                            # wants to join
                                                            # to the list
    # If there is NOT a party going on...
    else: 
        party_started = True    # Set that we are in a party
        party_channel = ctx.message.channel     # Set the channel it's in
        party_name_list[0]=(str(ctx.message.author.id)) # Get the first 
                                                        # partygoer in the list

# r.partystart (the start command)
@client.command()
async def partystart():
    # Set our globals
    global party_name_list
    global party_channel
    global party_started

    # Only continue if there is a party
    if party_started == True:
        party_counter=len(party_name_list)  # Find how long our name list is

        # A little flavor to start things off
        await client.send_message(party_channel,("""HUGS FOR EVERYONE!\n"""
        """https://pre00.deviantart.net/2b7f/th/pre/i/2018/306/d/a/"""
        """hug_ralsei_by_mcwitherzberry-dcqynhd.png"""))
        
        # After the flavor, mention everyone who joined
        for i in range(0, party_counter): 
            await client.send_message(party_channel,
            "<@%s> " % party_name_list[i])
        
        # After that, we destroy the list
        for i in range(0, party_counter):
           party_name_list.pop(0)

        # The bot says a final love you
        await client.send_message(party_channel, ("""I :heart: you all!"""))

        # We re-instate the list
        party_name_list = [""]

        # And set the party status to false
        party_started = False
    
    # If there is not a party, do nothing
    else:
        return 

# Actually run the bot with the token from earlier, stripping it of 
# unnecessary characters, specifically newlines
client.run(token.strip())


